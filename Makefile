INSTALL_DIR ?= $(HOME)/.local/bin

build:
	mkdir -p build/
	pip3 wheel -w build/ .

dist: build
	mkdir -p dist/
	pex --python=python3 -f ./build gpt_code_helper -e gpt_code_helper.main:main -o dist/code-helper

install: dist
	mkdir -p $(INSTALL_DIR)
	cp ./dist/code-helper $(INSTALL_DIR)

clean:
	rm -rf dist/ build/
