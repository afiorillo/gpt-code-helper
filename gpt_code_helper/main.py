#!/usr/bin/env python
"""
Copyright (c) 2023 Andrew Fiorillo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from collections import defaultdict
from decimal import Decimal
from enum import Enum
from hashlib import sha256 as hashfunc
from pathlib import Path
import logging

import openai
from pydantic import BaseModel
from rich import print
from rich.progress import Progress, SpinnerColumn, TextColumn
import typer
from typing_extensions import Annotated

from gpt_code_helper.settings import load_settings

log = logging.getLogger("gpt-code-helper")
logging.basicConfig(level=logging.INFO, format='[%(levelname)-8s] %(message)s')

USER_PROMPT = "What do you want GPT's help with? ['q' to quit, 'm' to change model, `r` to repeat]"

SYSTEM_PROMPT = """
Help with a coding session. Keep your responses brief with minimal explanation, focus on the specific changes needed to answer.
""".strip()

PROMPT_TEMPLATE = """
Here is my current code:
{code_to_share}
I want you to help me with:
{prompt}
""".strip()

def format_contained_files(file_or_directory: Path, include_nontext: bool = False):
    """Reads the contained contents and formats them nicely for GPT. For example

      ./example
      ```sh      
      echo hello world
      ```

      ./python/example.py
      ```py
      print("hello world")
      ```
    """
    def _fmt_file_as_lines(file: Path, root: Path) -> list[str]:
        return [
            f"./{file.relative_to(root)}",
            f'```{file.suffix.lstrip(".")}',
            file.read_text(errors=('ignore' if include_nontext else 'strict')),
            '```',
            '\n'
        ]

    if file_or_directory.is_dir():
        root = file_or_directory
        all_files = sorted([fp for fp in file_or_directory.rglob("*")])
    else:
        root = file_or_directory.parent
        all_files = [file_or_directory]

    lines = []
    for fp in all_files:
        if fp.is_file():  # because the rglob might catch dirs
            lines.extend(_fmt_file_as_lines(fp, root))

    return '\n'.join(lines)

def calculate_directory_hash(file_or_directory: Path) -> str:
    """Calculates the checksum of the given file deterministically."""
    checksum = hashfunc()

    if file_or_directory.is_dir():
        all_files = sorted([fp for fp in file_or_directory.rglob("*")])
    else:
        all_files = [file_or_directory]

    for fp in all_files:
        if not fp.is_file():
            continue
        checksum.update(str(fp.relative_to(file_or_directory)).encode())
        checksum.update(fp.read_bytes())
    return checksum.hexdigest()

class _ChatCompletionUsage(BaseModel):
    prompt_tokens: int
    completion_tokens: int
    total_tokens: int

class _ChatCompletionMessage(BaseModel):
    role: str
    content: str
    
class _ChatCompletionChoices(BaseModel):
    message: _ChatCompletionMessage
    finish_reason: str
    index: int

class _ChatCompletionResponse(BaseModel):
    usage: _ChatCompletionUsage
    choices: list[_ChatCompletionChoices]

    @property
    def first_choice(self) -> str:
        return self.choices[0].message.content

MODEL_PRICE_PER_K_TOKEN = {
    'gpt-3.5-turbo': 0.0002,
    'gpt-4': 0.06,
    'gpt-4-32k': 0.12,
}

def calculate_cost(tokens_used: dict) -> Decimal:
    total_cost = Decimal(0.0)
    for model_used, tokens in tokens_used.items():
        dollars_per_1000_tokens = Decimal(MODEL_PRICE_PER_K_TOKEN.get(model_used, float('nan')))
        total_cost += Decimal(tokens)/Decimal(1000) * dollars_per_1000_tokens
    return total_cost

def session(file_or_directory: Annotated[str, typer.Argument(help="A file or folder to share with GPT. If given a folder, all contained files will be shared.")]):
    """
    Start an interactive session with GPT, given some input files.
    """
    current_settings = load_settings()

    log.debug(f"given: {file_or_directory}")

    def _new_message_history() -> list:
        return [
            {"role": "system", "content": SYSTEM_PROMPT},
        ]

    current_model = current_settings.MODEL_NAME
    tokens_used = defaultdict(int)  # model_name: tokens
    messages = _new_message_history()

    code_to_share = ''  # rewritten on the first iteration
    code_hash = '' # rewritten on the first iteration
    prompt = "ignored, rewritten on first loop"
    while prompt.strip() != 'q':
        prompt = typer.prompt(USER_PROMPT, prompt_suffix='\n')
        if prompt.strip() in ['m', 'model']:
            print("Available models (USD per 1k tokens):", MODEL_PRICE_PER_K_TOKEN)
            current_model = typer.prompt("What model would you like to use?")
            while current_model not in MODEL_PRICE_PER_K_TOKEN:
                current_model = typer.prompt(f"Given model `{current_model} not available. What model would you like to use?")
            continue
        elif prompt.strip() in ['q', 'quit']:
            continue  # and we exit on the next check
        elif prompt.strip() in ['r', 'repeat']:
            # this one is useful if you want to rerun the previous messages but with a
            # different model, so you use `m` to change and then `r` to repeat the prompt
            continue

        # otherwise keep the conversation going
        new_hash = calculate_directory_hash(Path(file_or_directory))
        if code_hash == new_hash:
            # no code has changed, so we can build on previous messages
            messages.append({"role": "user", "content": prompt})
        else:
            messages = _new_message_history()
            code_to_share = format_contained_files(Path(file_or_directory), current_settings.SHARE_BINARY_FILES)
            code_hash = new_hash
            messages.append({"role": "user", "content": PROMPT_TEMPLATE.format(code_to_share=code_to_share, prompt=prompt)})

        if len(code_to_share) > current_settings.WARN_CODE_LENGTH:
            proceed = typer.confirm(f"Your code contains {len(code_to_share)} chars. Are you sure you want to proceed?")
            if not proceed:
                continue  # and go back to the original loop

        with Progress(
            SpinnerColumn(),
            TextColumn("[progress.description]{task.description}"),
            transient=True
        ) as progress:
            progress.add_task(description="Calling OpenAI...", total=None)
            resp = openai.ChatCompletion.create(model=current_model, messages=messages)
        resp = _ChatCompletionResponse.parse_obj(resp)
        tokens_used[current_model] += resp.usage.total_tokens

        print('\nBot Answer:')
        print(resp.first_choice + '\n')

        if current_settings.SHOW_COST_EVERY_TIME:
            print(f"Total cost so far: {calculate_cost(tokens_used):.8f} USD")

    print(f'Total Session Cost: {calculate_cost(tokens_used):.8f} USD')

def main():
    # the entrypoint for pex
    typer.run(session)

if __name__ == '__main__':
    main()
