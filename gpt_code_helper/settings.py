from pathlib import Path

from pydantic import BaseSettings, Field

CONFIG_FILE = Path.home().joinpath('.config/code-helper/settings.json')

class Settings(BaseSettings):
    """ Expose these as environment variables when running the script."""
    OPENAI_API_KEY: str
    OPENAI_ORG_ID: str | None = Field(
        default=None,
        description="If your OpenAI user has multiple organizations, you should specify which organization to use and bill to."
    )
    MODEL_NAME: str = Field(
        default='gpt-3.5-turbo',
        description="The OpenAI model to default to. This can be changed mid-session."
    )
    SHOW_COST_EVERY_TIME: bool = Field(
        default=True,
        description="If True, prints the current session cost before every prompt."
    )
    WARN_CODE_LENGTH: int = Field(
        default=5000,
        description="The character threshold, above which a given prompt will trigger an 'are you sure?' confirmation. This is primarily aiming to mitigate the risk of accidentally sharing a whole `.git` folder or something similar, which could be needlessly expensive and reduce performance."
    )
    SHARE_BINARY_FILES: bool = Field(
        default=False,
        description="If True, will send binary (non-UTF8 encoded) files to GPT along with the rest. Default behavior is to abort in such cases (since GPT really isn't meant to parse binary files)."
    )

def load_settings(config_file: Path = CONFIG_FILE):
    """
    Reads settings from a file if possible, then falls back to environment vars.
    If a requried setting is not set, this will fail.
    """
    if config_file.exists():
        return Settings.parse_file(config_file)
    return Settings()
