from distutils.core import setup

setup(
    name='gpt-code-helper',
    version='1.0',
    description='A wrapper around GPT models to help with iterative coding sessions.',
    author='Andrew Fiorillo',
    author_email='andrew@0x6166.com',
    packages=['gpt_code_helper'],
)