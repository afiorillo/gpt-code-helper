# gpt-code-helper

A smallish wrapper around GPT which provides your up-to-date code to provide specific suggestions.
If the code doesn't change, subsequent prompts just follow up on the original message (saving tokens).
If the code does change, then the message history gets reset.

## Developing

This project uses Poetry to manage packages.
Assuming you have it installed (and Python ^3.10):

```bash
$ poetry install
$ python gpt_code_helper/main.py --help
```

### Pex Binary

This package also can be used as a self-contained Pex binary.

```bash
$ ./build.sh
# copy it somewhere on your path
$ cp dist/code-helper $HOME/.local/bin/code-helper
$ code-helper --help
```

### GNU Make

This package comes with a Makefile to simplify building, installation, and updates.
To build and install the Pex binary:

```bash
# defaults to `$HOME/.local/bin/` as the installation directory
$ make install
# alternatively, you can specify a custom directory but your user will need permissions to
# write there
$ INSTALL_DIR="$HOME/bin" make install
```
