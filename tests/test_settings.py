from pathlib import Path
from tempfile import NamedTemporaryFile

from gpt_code_helper.settings import load_settings

def test_min_settings_file():
    fp = NamedTemporaryFile(mode="w")
    fp.write("""{"OPENAI_API_KEY": "foo"}""")
    fp.seek(0)

    # and now loading it should pass
    settings = load_settings(Path(fp.name))
    assert settings.OPENAI_API_KEY == 'foo'

    fp.close()

def test_settings_file():
    fp = NamedTemporaryFile(mode="w")
    fp.write("""{"OPENAI_API_KEY": "foo", "SHOW_COST_EVERY_TIME": false}""")
    fp.seek(0)

    # and now loading it should pass
    settings = load_settings(Path(fp.name))
    assert settings.OPENAI_API_KEY == 'foo'

    fp.close()