#!/usr/bin/env python
"""
This could be replaced with pytest, but in lieu of internet connectivity here is a helper
script for running all the tests. The repository root needs to be in the PYTHONPATH. E.g.

```bash
PYTHONPATH="$(git rev-parse --show-toplevel)" python tests/run_tests.py
```
"""
from typing import Callable

from tests import test_code_preparation, test_settings

def get_test_funcs(test_module) -> list[Callable]:
    return [func for funcname, func in test_module.__dict__.items() if funcname.startswith("test_")]

TEST_FUNCS = [
    *get_test_funcs(test_code_preparation),
    *get_test_funcs(test_settings),
]

for fn in TEST_FUNCS:
    try:
        modname = fn.__module__
        name = fn.__name__
        fn()
    except Exception as err:
        print(f"❌ {modname}.{name} : {err}")
    else:
        print(f"✅ {modname}.{name}")