from pathlib import Path
from textwrap import dedent

from gpt_code_helper.main import format_contained_files

TESTS_DIR = Path(__file__).parent
EXAMPLES_DIR = TESTS_DIR.joinpath("examples")

def test_happy_single_file():
    fp = EXAMPLES_DIR.joinpath('c.md').resolve()
    code_to_share = format_contained_files(fp)
    expected = (
"""./c.md
```md
This is a file next to `directory1`.
```

""")
    assert code_to_share.splitlines() == expected.splitlines(), "shared code is different!"
    

def test_happy_single_directory():
    fp = EXAMPLES_DIR.joinpath("directory1").resolve()
    code_to_share = format_contained_files(fp)
    expected = (
"""./a.md
```md
This is one of the files in `directory1`.
```


./b.md
```md
This is another file in `directory1`.
```

""")
    assert code_to_share.splitlines() == expected.splitlines(), "shared code is different!"

def test_happy_single_parent_directory():
    fp = EXAMPLES_DIR.resolve()
    code_to_share = format_contained_files(fp)
    expected = (
"""./c.md
```md
This is a file next to `directory1`.
```


./directory1/a.md
```md
This is one of the files in `directory1`.
```


./directory1/b.md
```md
This is another file in `directory1`.
```


./directory2/d.md
```md
This is the only file in `directory2`.
```

""")
    assert code_to_share.splitlines() == expected.splitlines(), "shared code is different!"
